---
layout: post
title: "Good Food inevitable for Good Health"
author: "Santhosh George"
last_modified_at: "Sep 3, 2021"
date: "August 24, 2021"
read_time: "4 min"
img: "/assets/images/b3.webp"
---

I remember visiting my senior teacher in Kolkata long ago! Early evening my

teacher’s wife offered me some sweet biscuits, fried snacks and milk tea with sugar.

She said ‘all these are for you and I will bring some other food for my husband as per

his doctor’s prescription’. My teacher was recovering from a heart surgery. She

brought some chickpeas sprouts with cut onions, green chili and some cut fruits.

Instead of tea with milk and sugar, she gave him some tea with lemon. At that point

in time, around 25 years ago, I felt sad for my teacher and thought what a pity that

he can’t take all these ‘sweet and fried’ food. But later as I worked on finding ‘good

food for good health’ and ways to escape lifestyle diseases, I discovered that sprout

chickpeas and tea with lemon and honey are very good for health. So I decided to

change my food immediately, instead of waiting for my heart to breakdown and

undergo surgery and then start a healthy diet with good food. I felt so good after I

began taking good food even when I am still healthy! I realised that good food

guarantees good health and rich food often leads to poor health.

And let me tell you! Once we start taking fruits and sprouts and lemon tea with honey

its heaven! I don’t miss oily and sugary snacks anymore. Good food taste much

better than the sweetest biscuits or the oily snacks. I discovered that there are

varieties of good food available and they are so tasty and amazing. Eat pulses and

nuts and fruits and vegetables and avoid fried chips, fast food and all junk food.

Since we are so conditioned to eating unhealthy food for so many years, we have to

take a conscious decision to make this positive change. Also the problem is that

since unhealthy food is available in plenty everywhere and healthy food is

comparatively not marketed that well you have to take that extra step for great

health.

First let’s understand that we are talking about good physical body with the right

weight and energy level as an outcome of consuming good food. We are not looking

for an extraordinary body that need extraordinary diet with extraordinary efforts to

sustain that heavy built muscle body. We are not suggesting an expensive lifestyle

where one has to spend a lot of money on artificial energy and protein drink and

heavy membership fee on fitness centres. Rather we are looking for normal human

beings with normal body with normal fitness with normal weight with normal diet with

normal lifestyle, whose weight matches with their height. What we don’t want is

below normal body with unhealthy weight and less energy.

We want to see more people live fit and healthy and integrate certain lifestyle

modifications at an early stage when the health is still under control. It’s very true for

children who are fit and healthy. Ignorance of parents should not put the health of

children at risk. Parents should know what food is good for their children. Careless

feeding of unhealthy food will lead children gaining unhealthy weight and lifestyle


diseases at a very early stage in life. We do not want juvenile diabetes to explode.

We have to put a stop to unhealthy weight among children soon as possible.

Ultimately HealthAshram look for an inspired generation of wise people who

understand body and food well and ensure wellbeing of it and not take our health for

granted.

When it comes to Good Food, what you eat, when you eat, how much you eat and

how you eat truly matter a lot. Today majority people decide on their choice of food

based on what they see in the menu. Instead of looking at the menu what you should

do is to go with what you see in the tree and plants around you. Don’t go with what is

there for sale and offer. Go with what is there ready for the harvest. Never eat food

based on your buying capacity but always eat food based on your burning

capacity. If you have the habit of eating more food than what you require, you are

sure fallen in the path to gaining unhealthy weight that leads to lifestyle diseases and

lifetime medicines.

<img height="500" src="{{site.baseurl}}{{page.img}}">

With super market and home delivery of all kinds of packed and preserved food, we

have to be very alert not to become super fools. Please use your wisdom and ask

yourself a question. Am I wise enough in deciding the right food to eat? Am I using

my common sense while choosing a drink? Please remember that your body is

not a waste disposal bin. Your mouth is not a waste disposal pit. Don’t just

through whatever comes on your way.

1. Don’t just go with what your eyes adore and your tongue linger. Rather go

with what your pancreas can handle and your lever can absorb and what your

body can burn.

2. Go with the energy you require for the day and not what your wallet can

afford.

3. Don’t eat to impress your friends; eat to live healthy and happy.

Can we eat anything and everything we find around us? No. No. No. So always

avoid packed, preserved and processed food. Say no to sugary &amp; aerated drinks

with artificial color and flavor. Next time you look at the packets carefully and you

will see printed in very small letters, ‘permitted preservatives’ added. My question is

who gave permission to add these preservative chemicals in our food? In fact we

should ask our liver and kidneys and pancreas for their permission before dumping

any such artificial color and flavor. I am sure they will say a big no! Ask your heart

before consuming too much of fried food with artificial color and flavor.

Remember always good food is inevitable for good health. 


Join HealthAshram and start a new lifestyle to stay away from lifestyle diseases and lifetime medicines.


Santhosh George

HealthAshram
