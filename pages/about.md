---
layout: page
title: About
permalink: "/about/"
image1: "/assets/images/founder.webp"
---

<h1>Health Ashram</h1>

“The best solution for many problems is Prevention”

Many problem people face today can be prevented. It’s not worth waiting for children and elderly reach the problem level and then start working on a relief. It’s not worth losing one’s health and then work on fitness to regain it. It’s not worth putting on too much of unhealthy weight and then work on losing it. It’s not worth children growing with disability and then work on rehabilitating them. What can be prevented should be prevented at the right time with the right method. At the We10 foundation our focus is on prevention of many problems people face today.

<h1>Our Motto</h1>

Together ‘WE’ can achieve more. Not alone. Not a ‘personal’ struggle but a united movement. The priority is also not a personal profit but ‘public prosperity’ is the highest priority in this initiative


<h1>Meet Our Founder</h1>

<img height="500" src="{{site.baseurl}}{{page.image1}}">

Teacher

Santosh George
Founder of WE10. He has been in social service for many decades.

